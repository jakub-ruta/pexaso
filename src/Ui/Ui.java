/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ui;

import game.privat.Game;
import game.privat.GamePlay;
import java.util.Scanner;

/**
 *
 * @author jakub
 */
public class Ui {

    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Hra pexaso začíná");
        System.out.println("Zadej pocet hracu");
        int nPlayers = sc.nextInt();
        System.out.println("Zadej pocet karet");
        int nCards = sc.nextInt();
        GamePlay game = new Game(nPlayers, nCards, "hra");
        
        while (game.nextplayer() != -1){
            int hrajiciHrac = game.nextplayer();
            System.out.println("Na řadě je hráč: " + hrajiciHrac);
            System.out.println(game.toString(4,""));
            System.out.println("Kterou kartu chceš otočit?");
            int karta1 = sc.nextInt();
            System.out.println("Otočil jsi kartu s tímto obrázkem: " + game.whatIsUnder(karta1) );
            System.out.println("Kterou druhou kartu chceš otočit?");
            int karta2 = sc.nextInt();
            System.out.println("Otočil jsi kartu s tímto obrázkem: " + game.whatIsUnder(karta2) );
            boolean uhadnulJsem = game.uhadnulJsem(hrajiciHrac, karta1, karta2);
            
            System.out.println(uhadnulJsem?"Uhádnul jsi":"Neuhádnul jsi");
            
        }
        
        System.out.println("Vyhrál hráč s číslem " + game.getWinner() + " s počtem uhádnutých " + game.getUhadnutych(game.getWinner()));
        System.out.println("Hra skončila");
        
    }
    
   

}
