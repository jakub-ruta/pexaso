/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.privat;

import java.util.Random;

/**
 *
 * @author jakub
 */
public class Game implements GamePlay{

    private final int pocetHracu;
    private final int pocetKaret;
    private players[] players;
    private final String nazevHry;
    private int[][] poleKaret;
    private int whoPlaying = 1;
    private boolean shouldRepeatPlay;
    private int pocteUhadnutychCelkem;

    public Game(int countPlayers, int pocetKaret, String nazevHry) {
        this.pocetHracu = countPlayers;
        this.pocetKaret = pocetKaret;
        this.nazevHry = nazevHry;
        generujPexaso();
        zalozHrace();
    }

    private void zalozHrace() {
        this.players = new players[pocetHracu];
        for (int i = 0; i < pocetHracu; i++) {
            //String name = sc.next();
            
            players[i] = new players(i, "test-" + i);
        }
    }

    private void generujPexaso() {
        this.poleKaret = new int[2][pocetKaret];
        int k = 0;
        for (int i = 1; i < (pocetKaret / 2) + 1; i++) {
            for (int j = 0; j < 2; j++) {
                poleKaret[0][k + j] = i;
            }
            k = k + 2;
        }
        fisherYates(poleKaret[0]);
    }

    @Override
    public int getUhadnutych(int playerId) {
        return players[playerId].getUhadnutych();
    }

    @Override
    public int nextplayer() {
        if (pocteUhadnutychCelkem == (pocetKaret / 2)) {
            return -1;
        }
        return whoPlaying;
    }

    private void jumpToNextPlayer() {

        if (whoPlaying == pocetHracu) {
            whoPlaying = 1;
        } else {
            whoPlaying = whoPlaying + 1;
        }

    }

    @Override
    public boolean uhadnulJsem(int playerId, int positionA, int positionB) {
        assert positionA > pocetKaret-1 : "Chyba";
        
        if ((positionA == positionB) || positionA > pocetKaret-1
                || positionB > pocetKaret-1 || positionA < 0 || positionB < 0
                || poleKaret[1][positionA] == 1 || poleKaret[1][positionB] == 1) {

            shouldRepeatPlay = false;
            jumpToNextPlayer();
            return false;
        } else if (poleKaret[0][positionA] == poleKaret[0][positionB]) {
            poleKaret[1][positionA] = 1;
            poleKaret[1][positionB] = 1;
            shouldRepeatPlay = true;
            pocteUhadnutychCelkem ++;
            players[playerId-1].zvysitPocetUhadnutych();

            return true;
        } else {
            shouldRepeatPlay = false;
            jumpToNextPlayer();
            return false;

        }
    }

    @Override
    public int getWinner() {
        int maxId = 0;
        int max = 0;
        for (int i = 0; i < pocetHracu; i++) {
            if (players[i].getUhadnutych() > max) {
                max = players[i].getUhadnutych();
                maxId = i;
            }
        }
        return maxId+1;
    }

    @Override
    public int[] getPoleKaret() {
        return poleKaret[1];
    }

  /*  @Override
    public String toString(int pocetSloupcu) {
        StringBuilder karty = new StringBuilder();
        for (int i = 1; i < pocetKaret + 1; i++) {
            karty.append(poleKaret[1][i - 1]).append(" ");
            if (i % pocetSloupcu == 0 && i != 0) {
                karty.append("\n");
            }
        }
        return karty.toString();
    }*/
    @Override
     public String toString(int pocetSloupcu, String param) {
        StringBuilder karty = new StringBuilder();
        for (int i = 1; i < pocetKaret + 1; i++) {
            if (poleKaret[1][i - 1] == 0){
                if (i > 10){
                    karty.append(i-1).append(" ");
                }
                else{
                    karty.append(i-1).append("  ");
                }
                
            }
            else {
                karty.append("x  ");
            }
            
            if (i % pocetSloupcu == 0 && i != 0) {
                karty.append("\n");
            }
        }
        return karty.toString();
    }
     
    @Override
    public String whatIsUnder(int position){
        return  "" + poleKaret[0][position];
    }

    /**
     * An improved version (Durstenfeld) of the Fisher-Yates algorithm with O(n)
     * time complexity Permutes the given array
     *
     * @param array array to be shuffled
     *
     * downloaded fron internet
     */
    private static void fisherYates(int[] array) {
        Random r = new Random();
        for (int i = array.length - 1; i > 0; i--) {
            int index = r.nextInt(i);
            //swap
            int tmp = array[index];
            array[index] = array[i];
            array[i] = tmp;
        }
    }

}
