/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.privat;

/**
 *
 * @author jakub
 */
public interface GamePlay {

    /**
     * Vrátí počet uhádnutých karet pro zadané id hráče
     *
     * @param playerId id hráče
     * @return vrátí int uhádnutých
     */
    public int getUhadnutych(int playerId);

    /**
     * vrátí id hráče na ředě
     *
     * @return int id hráče
     */
    public int nextplayer();

    /**
     * zjistí zda josu karty stejné
     *
     * @param playerId id hráče
     * @param positionA karta A
     * @param positionB karta B
     * @return Jsou karty stejné?
     */
    public boolean uhadnulJsem(int playerId, int positionA, int positionB);

    /**
     * pole karet
     *
     * @return prací pole karet
     */
    public int[] getPoleKaret();

    /**
     * převdení pole na string s definovaným počtem slouapců
     *
     * @param pocetSloupcu počet sloupců
     * @param param nepoužito
     * @return vrací String pole karet
     */
    public String toString(int pocetSloupcu, String param);

    /**
     * vratí co je to za kartu
     *
     * @param position číslo karty
     * @return jaká to je karta String
     */
    public String whatIsUnder(int position);

    /**
     * vrátí id vítěze
     *
     * @return id vítěze
     */
    public int getWinner();

}
